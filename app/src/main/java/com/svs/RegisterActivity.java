package com.svs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;


public class RegisterActivity extends AppCompatActivity {

    TextView lblSignIn;
    EditText txtUsernameRegister, txtEmailRegister, txtHallTicketRegister, txtMobileRegister;
    boolean flag = false;
    Button btnReg;


    private String regUsernameValue;
    private String regEmailValue;
    private String regHallValue;
    private String regMobileValue;


    private ProgressDialog pDialog;
    JSONArray operators = null;
    private String verificationTextGet, name, email;
    ServiceHandler sh;


    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,100}" + "@"
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,10}" + "(" + "."
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,20}" + ")+");
    private static final Pattern USERNAME_PATTERN = Pattern
            .compile("[a-zA-Z0-9]{1,250}");
    private static final Pattern PASSWORD_PATTERN = Pattern
            .compile("[a-zA-Z0-9+_.]{4,16}");
    private static final String REQUIRED_MSG = "required";
    private static final String VALID_EMAIL_MSG = "Enter Valid Email";
    private static final String VALID_PASSWORD_MSG = "Enter Valid Password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        try {
            getSupportActionBar().hide();

            txtUsernameRegister = (EditText) findViewById(R.id.txtUsernameRegister);
            txtEmailRegister = (EditText) findViewById(R.id.txtEmailRegister);
            txtHallTicketRegister = (EditText) findViewById(R.id.txtHallTicketRegister);
            txtMobileRegister = (EditText) findViewById(R.id.txtMobileRegister);
            lblSignIn = (TextView) findViewById(R.id.lblSignIn);
            btnReg = (Button) findViewById(R.id.btnRegister);

            btnReg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    regUsernameValue = txtUsernameRegister.getText().toString();
                    regEmailValue = txtEmailRegister.getText().toString();
                    regHallValue = txtHallTicketRegister.getText().toString();
                    regMobileValue = txtMobileRegister.getText().toString();

                    txtUsernameRegister.setError(null);
                    txtEmailRegister.setError(null);
                    txtHallTicketRegister.setError(null);
                    txtMobileRegister.setError(null);

                    if (regUsernameValue.length() == 0) {
                        txtUsernameRegister.setError(REQUIRED_MSG);

                    }

                    if (regEmailValue.length() == 0) {
                        txtEmailRegister.setError(REQUIRED_MSG);

                    }
                    if (regHallValue.length() == 0) {
                        txtHallTicketRegister.setError(REQUIRED_MSG);

                    }
                    if (regMobileValue.length() == 0) {
                        txtMobileRegister.setError(REQUIRED_MSG);

                    }


                    //****************************


                    if (!regUsernameValue.equals("") && !regEmailValue.equals("")
                            && !regHallValue.equals("")
                            && !regMobileValue.equals("")) {


                        if (!CheckEmail(regEmailValue)) {
                            txtEmailRegister.setError(VALID_EMAIL_MSG);
                        } else {


                            txtUsernameRegister.setText("");
                            txtEmailRegister.setText("");
                            txtHallTicketRegister.setText("");

                            txtMobileRegister.setText("");

                            // strReplace(regNameValue);
                            // strReplace(regEmailValue);
                            // strReplace(regUsernameValue);
                            // strReplace(regPasswordValue);
                            // strReplace(regAddressValue);
                            new SendingRegistrationDetails().execute();
                            // Intent i = new
                            // Intent(RegisterActivity.this,LoginActivity.class);
                            // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            // i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            // startActivity(i);
                        }
                        if (!CheckPassword(regHallValue)) {
                            txtHallTicketRegister.setError(VALID_PASSWORD_MSG);
                        }
                    }

                    //****************************


                }
            });
            lblSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean CheckEmail(String email) {

        return EMAIL_PATTERN.matcher(email).matches();
    }

    private boolean CheckPassword(String password) {

        return PASSWORD_PATTERN.matcher(password).matches();
    }

    private boolean CheckUsername(String username) {

        return USERNAME_PATTERN.matcher(username).matches();
    }


    // Sending Data
    private class SendingRegistrationDetails extends
            AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

            pDialog = new ProgressDialog(RegisterActivity.this);
            pDialog.setMessage("Registering...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            // deleteCategoryData();
            // deleteProductData();

            try {

                String add = regHallValue + "&fname=" + regUsernameValue + "&mno=" + regMobileValue + "&email=" + regEmailValue;

                add = add.replace(" ", "%20");
                //add = add.replace("&", "%26");
                add = add.replace("'", "%27");
                add = add.replace("(", "%28");
                add = add.replace(")", "%29");
                add = add.replace("-", "%2D");
                add = add.replace(".", "%2E");
                add = add.replace(":", "%3A");
                add = add.replace(";", "%3B");
                add = add.replace("?", "%3F");
                add = add.replace("@", "%40");
                add = add.replace("_", "%5F");

                String RegisUrl = "http://logicupsolutions.com/svsalumni/signup.php?hno="
                        + add;

                sh = new ServiceHandler();

                String jsonStrSendverifyCode = sh.makeServiceCall(RegisUrl,
                        ServiceHandler.GET);

                Log.d("User: ", "> " + jsonStrSendverifyCode);


            } catch (Exception e) {

                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            if(flag==true)
            {
                Toast.makeText(getApplicationContext(),
                        "Registered Sucessfully", Toast.LENGTH_LONG).show();
            }

        }

    }
}
